# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
# require "active_record/railtie"
# require "active_storage/engine"
require 'action_controller/railtie'
require 'action_mailer/railtie'
# require "action_mailbox/engine"
# require "action_text/engine"
require 'action_view/railtie'
require 'action_cable/engine'
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module UrlShortener
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1

    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w[assets tasks])

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    config.time_zone = 'UTC'
    # config.eager_load_paths << Rails.root.join("extras")
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}')]
    # Don't generate system test files.
    config.generators.system_tests = nil

    if Rails.env.production?
      Sidekiq.configure_server do |config|
        config.redis = { url: ENV['REDIS_URL'], network_timeout: 5, pool_timeout: 5 }
      end
      Sidekiq.configure_client do |config|
        config.redis = { url: ENV['REDIS_URL'], network_timeout: 5, pool_timeout: 5 }
      end
    elsif Rails.env.development?
      Sidekiq.configure_server do |config|
        config.redis = { url: ENV['REDIS_URL'] }
      end
      Sidekiq.configure_client do |config|
        config.redis = { url: ENV['REDIS_URL'] }
      end
    end
  end
end
