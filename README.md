# README

> This is a Rails application to shorten long URLs

## Installation

1. **Prerequisites:**
    * Ruby version 3.3.0
    * Bundler gem (2.5.14)
    * MongoDB
    * Redis
    * Docker

2. **Instructions:**
    * Git clone repository from [URL shortener](https://gitlab.com/jxsim1/url_shortener)
    * Ensure bundler is installed `gem install bundler -v 2.5.14` and run `bundle` to install all gem dependencies
    * Ensure MongoDB and Redis are installed and started
        * Included in docker-compose.yml

3. **Commands:**
    * Unit test: `rspec`
    * Lint: `rubocop`
    * Security tool: `docker compose up brakeman'
    * Run app (rails server + tailwindcss + sidekiq): `./bin/dev`
    * Setup database: `rails db:setup`
    * Generate report `rake report:generate_url_lookup`

4. **Deployment:**
    * Create merge request to `main` branch
        * Auto deploy to dokku

5. **Environment**
    * Production: `https://u.jxsim.me/`