namespace :report do
  task generate_url_lookup: :environment do
    UrlLookupReportService.new.process_report
  end

  task generate_url_lookup_async: :environment do
    Report::UrlLookupReportJob.perform_async
  end
end
