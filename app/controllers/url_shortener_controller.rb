class UrlShortenerController < ApplicationController
  def show
    @url_lookup = UrlLookupGetService.new.get_url_lookup(short_id: params[:short_id])

    if @url_lookup.nil?
      flash[:error] = I18n.t('errors.url_lookup.not_found')
      return redirect_to root_path
    end

    UrlLookupMetadataService.new.record_visit(url_lookup: @url_lookup, ip_address: request.remote_ip)

    redirect_to @url_lookup[:target_url], allow_other_host: true
  end

  def new
    @created_url_lookup = if params.key?(:created_short_url)
                            UrlLookupGetService.new.get_url_lookup(short_url: params[:created_short_url])
                          end
    @url_lookup = UrlLookup.new
  end

  def create
    create_service = UrlLookupCreateService.new(create_url_lookup_params)

    unless create_service.valid?
      @url_lookup = create_service.url_lookup
      render :new, status: :bad_request
      return
    end

    created_url_lookup = create_service.create!

    redirect_to root_path(created_short_url: created_url_lookup[:short_url])
  end

  private

  def create_url_lookup_params
    params.require(:url_lookup).permit(:target_url)
  end
end
