class UrlLookup
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :url_lookup_metadatas, dependent: :destroy

  field :short_url, type: String
  field :target_url, type: String
  field :title, type: String

  validates :short_url, uniqueness: { allow_nil: true, message: I18n.t('errors.url_lookup.short_url.uniqueness') }
  validates :target_url, presence: true, on: %i[create_form create]
  validate :validate_target_url_format, on: [:create_form]
  validate :validate_target_url_ok, on: [:create_form]

  index({ short_url: 1 }, { unique: true, name: 'short_url_index' })

  def validate_target_url_format
    return if target_url.blank?

    uri = URI.parse(target_url)
    errors.add(:target_url, I18n.t('errors.url_lookup.target_url.format')) unless uri.is_a?(URI::HTTP) && !uri.host.nil?
  rescue URI::InvalidURIError
    errors.add(:target_url, I18n.t('errors.url_lookup.target_url.format'))
  end

  def validate_target_url_ok
    return if target_url.blank?

    res = Faraday.get(target_url)

    errors.add(:target_url, I18n.t('errors.url_lookup.target_url.invalid')) if res.blank? || res.status != 200
  rescue StandardError
    errors.add(:target_url, I18n.t('errors.url_lookup.target_url.invalid'))
  end
end
