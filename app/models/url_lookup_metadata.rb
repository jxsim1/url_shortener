class UrlLookupMetadata
  include Mongoid::Document

  belongs_to :url_lookup

  field :visited_at, type: DateTime
  field :ip_address, type: String
  field :country, type: String

  validates :visited_at, presence: true
  validates :ip_address, presence: true
end
