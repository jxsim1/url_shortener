class S3Service
  def initialize
    @s3 = Aws::S3::Client.new(region: ENV.fetch('AWS_REGION', nil),
                              credentials: Aws::Credentials.new(Rails.application.credentials.aws.access_key_id,
                                                                Rails.application.credentials.aws.secret_access_key))
  end

  def upload(key, body)
    @s3.put_object(bucket: ENV.fetch('AWS_BUCKET', nil), key:, body:)
  end
end
