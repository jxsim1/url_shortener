class UrlLookupCreateService
  attr_reader :url_lookup

  def initialize(body)
    @url_lookup = UrlLookup.new(target_url: body[:target_url])
    @cache_service = UrlLookupCacheService.new
  end

  def valid?
    @url_lookup.valid?(:create_form)
  end

  def create!
    @url_lookup.short_url = generate_uniq_short_url
    @url_lookup.title = parse_url_title(@url_lookup.target_url)
    @url_lookup.save!

    @cache_service.cache_url_lookup(@url_lookup)

    @url_lookup
  end

  private

  def generate_uniq_short_url
    short_url_key = nil
    tries = 0
    loop do
      short_url_key = "#{ENV.fetch('GATEWAY_URL', nil)}/#{SecureRandom.base58(8)}"
      tries += 1
      break unless UrlLookup.find_by(short_url: short_url_key).present? && tries < 5
    end
    raise StandardError, 'Unable to generate short url' if short_url_key.nil?

    short_url_key
  end

  def parse_url_title(url)
    res = Faraday.get(url)
    return nil if res.nil?
    return nil unless res.headers['content-type']&.include?('text/html')
    return nil unless res.body

    document = Nokogiri::HTML(res.body)
    document.css('title').text
  rescue StandardError
    nil
  end
end
