require 'csv'

class UrlLookupReportService
  REPORT_KEY = 'reports/url_lookup_report'.freeze
  ORDERED_COLUMNS = %i[short_url target_url ip_address country clicks].freeze
  HEADERS = {
    short_url: 'Short URL',
    target_url: 'Target URL',
    ip_address: 'IP Address',
    country: 'Country',
    clicks: 'No. Of Clicks'
  }.freeze

  def process_report
    Rails.logger.info('Start to process url lookup report...')

    csv_string = generate_report
    key = "#{REPORT_KEY}_#{Time.zone.now.strftime('%Y-%m-%dT%H:%M:%S')}.csv"

    S3Service.new.upload(key, csv_string)

    Rails.logger.info("Uploaded report to S3 - #{key}")
    Rails.logger.info('Finished processing url lookup report')
  end

  private

  def generate_report
    CSV.generate do |csv|
      csv << generate_header

      url_lookup_details.each do |lookup_detail|
        csv << generate_row(lookup_detail)
      end
    end
  end

  def generate_header
    ORDERED_COLUMNS.map { |col| HEADERS[col] }
  end

  def generate_row(lookup_detail)
    ORDERED_COLUMNS.map { |col| lookup_detail[col] }
  end

  def url_lookup_details
    UrlLookupMetadata.collection
                     .aggregate([{
                                  '$lookup': {
                                    from: 'url_lookups',
                                    localField: 'url_lookup_id',
                                    foreignField: '_id',
                                    as: 'url_lookup'
                                  }
                                },
                                 { '$unwind': '$url_lookup' },
                                 { '$group': {
                                   # group by short_url and ip address
                                   _id: {
                                     short_url: '$url_lookup.short_url',
                                     ip_address: '$ip_address'
                                   },
                                   short_url: { '$first': '$url_lookup.short_url' },
                                   target_url: { '$first': '$url_lookup.target_url' },
                                   ip_address: { '$first': '$ip_address' },
                                   country: { '$first': '$country' },
                                   clicks: { '$sum': 1 }
                                 } },
                                 { '$project': {
                                   _id: 0,
                                   short_url: 1,
                                   target_url: 1,
                                   ip_address: 1,
                                   country: 1,
                                   clicks: 1
                                 } }])
  end
end
