class UrlLookupMetadataService
  def record_visit(url_lookup:, ip_address:)
    result = Geocoder.search(ip_address).first
    country = result&.country

    UrlLookupMetadata.create(url_lookup:, ip_address:, country:, visited_at: Time.zone.now)
  end
end
