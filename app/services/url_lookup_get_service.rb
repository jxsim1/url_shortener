class UrlLookupGetService
  def initialize
    @cache_service = UrlLookupCacheService.new
  end

  def get_url_lookup(params)
    short_url = if params.key?(:short_id)
                  "#{ENV.fetch('GATEWAY_URL', nil)}/#{params[:short_id]}"
                elsif params.key?(:short_url)
                  params[:short_url]
                end

    cached_target_url = @cache_service.get_cached_target_url(short_url)
    return cached_target_url if cached_target_url.present?

    url_lookup = UrlLookup.find_by(short_url:)
    @cache_service.cache_url_lookup(url_lookup) if url_lookup.present?

    url_lookup
  end
end
