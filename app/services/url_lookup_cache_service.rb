class UrlLookupCacheService
  def get_cached_target_url(short_url)
    cache_key = "url_shortener:url_lookup:#{short_url}"
    Rails.cache.fetch(cache_key)
  end

  def cache_url_lookup(url_lookup)
    cache_key = "url_shortener:url_lookup:#{url_lookup.short_url}"
    Rails.cache.write(cache_key, url_lookup, expires_in: 1.hour)
  end
end
