module Report
  class UrlLookupReportJob
    include Sidekiq::Job

    def perform(*_args)
      UrlLookupReportService.new.process_report
    rescue StandardError => e
      Rails.logger.error('Error generating url lookup report', e.message)
    end
  end
end
