require 'rails_helper'

RSpec.describe UrlLookupMetadata do
  it { is_expected.to have_fields(:visited_at).of_type(DateTime) }
  it { is_expected.to have_fields(:ip_address, :country).of_type(String) }

  it { is_expected.to belong_to(:url_lookup) }
  it { is_expected.to validate_presence_of(:visited_at) }
  it { is_expected.to validate_presence_of(:ip_address) }
end
