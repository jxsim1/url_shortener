require 'rails_helper'

RSpec.describe UrlLookup do
  let(:target_url) { 'https://www.google.com' }
  let(:html_body) do
    <<-HTML
      <!DOCTYPE html>
      <html>
        <head>
          <title>Welcome!</title>
        </head>
        <body>
          <h1>Hello, world!</h1>
        </body>
      </html>
    HTML
  end

  before do
    stub_request(:get, target_url)
      .to_return(headers: { 'content-type': 'text/html; charset=UTF-8' }, body: html_body, status: 200)
  end

  it { is_expected.to have_timestamps }
  it { is_expected.to have_many(:url_lookup_metadatas) }
  it { is_expected.to have_fields(:short_url, :target_url, :title).of_type(String) }
  it { is_expected.to validate_uniqueness_of(:short_url).with_message('has already been taken') }
  it { is_expected.to validate_presence_of(:target_url).on(:create_form) }
  it { is_expected.to have_index_for(short_url: 1).with_options(unique: true, name: 'short_url_index') }

  describe '#validate_target_url_format' do
    let(:url_lookup) { build(:url_lookup, target_url:) }

    it do
      result = url_lookup.valid?(:create_form)

      expect(result).to be(true)
    end

    context 'when target url is of invalid format' do
      let(:target_url) { 'boo' }

      it do
        result = url_lookup.valid?(:create_form)

        expect(result).to be(false)
      end
    end
  end
end
