FactoryBot.define do
  factory :url_lookup_metadata do
    visited_at { Time.zone.now }
    ip_address { FFaker::Internet.ip_v4_address }
    country { FFaker::Address.country }
  end
end
