FactoryBot.define do
  factory :url_lookup do
    short_url { FFaker::Internet.http_url }
    target_url { FFaker::Internet.http_url }
    title { FFaker::Book.title }
  end
end
