require 'rails_helper'

RSpec.describe Report::UrlLookupReportJob, type: :job do
  subject { described_class.new }

  describe '#perform' do
    let(:report_service) { instance_double(UrlLookupReportService) }

    before do
      allow(UrlLookupReportService).to receive(:new).and_return(report_service)
      allow(report_service).to receive(:process_report)
    end

    it do
      subject.perform

      expect(report_service).to have_received(:process_report)
    end
  end
end
