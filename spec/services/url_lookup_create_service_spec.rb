require 'rails_helper'

RSpec.describe UrlLookupCreateService do
  subject { described_class.new(body) }

  let(:target_url) { 'https://www.google.com' }
  let(:body) { ActionController::Parameters.new({ target_url: }) }
  let(:html_body) do
    <<-HTML
      <!DOCTYPE html>
      <html>
        <head>
          <title>Welcome!</title>
        </head>
        <body>
          <h1>Hello, world!</h1>
        </body>
      </html>
    HTML
  end

  before do
    stub_request(:get, 'https://www.google.com')
      .to_return(headers: { 'content-type' => 'text/html; charset=UTF-8' }, body: html_body, status: 200)
  end

  describe '#valid?' do
    it { expect(subject).to be_valid }

    context 'when target url is blank' do
      let(:target_url) { '' }

      it do
        expect(subject.valid?).to be(false)
        expect(subject.url_lookup.errors.messages).to eq({
                                                           target_url: ["can't be blank"]
                                                         })
      end
    end

    context 'when target url is of invalid format' do
      let(:target_url) { 'boo' }

      before do
        stub_request(:get, target_url)
          .to_return(headers: { 'content-type': 'text/html; charset=UTF-8' }, body: html_body, status: 404)
      end

      it do
        expect(subject.valid?).to be(false)
        expect(subject.url_lookup.errors.messages).to eq({
                                                           target_url: ['is of invalid format', 'is not valid url']
                                                         })
      end
    end

    context 'when target url is not valid' do
      let(:target_url) { 'https://not-found.com' }

      before do
        stub_request(:get, target_url)
          .to_return(headers: { 'content-type': 'text/html; charset=UTF-8' }, body: html_body, status: 404)
      end

      it do
        expect(subject.valid?).to be(false)
        expect(subject.url_lookup.errors.messages).to eq({
                                                           target_url: ['is not valid url']
                                                         })
      end
    end
  end

  describe '#create!' do
    it do
      result = subject.create!

      expect(result).to be_an_instance_of(UrlLookup)
      expect(result[:target_url]).to eq(target_url)
      expect(result[:short_url]).to be_a(String)
      expect(result[:title]).to eq('Welcome!')
    end
  end
end
