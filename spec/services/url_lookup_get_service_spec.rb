require 'rails_helper'

RSpec.describe UrlLookupGetService do
  subject { described_class.new }

  let(:cache_service) { instance_double(UrlLookupCacheService) }
  let(:url_lookup) { build_stubbed(:url_lookup) }

  before do
    allow(UrlLookupCacheService).to receive(:new).and_return(cache_service)
    allow(cache_service).to receive(:get_cached_target_url).and_return(nil)
    allow(cache_service).to receive(:cache_url_lookup)
    allow(UrlLookup).to receive(:find_by).and_return(url_lookup)
  end

  describe '#get_url_lookup' do
    it do
      result = subject.get_url_lookup(short_id: 'abc')

      expect(result).to eq(url_lookup)
    end

    context 'when url lookup not found in db' do
      before do
        allow(UrlLookup).to receive(:find_by).and_return(nil)
      end

      it do
        result = subject.get_url_lookup(short_id: 'abc')

        expect(result).to be_nil
      end
    end

    context 'when url lookup is found in cache' do
      before do
        allow(cache_service).to receive(:get_cached_target_url).and_return(url_lookup)
      end

      it do
        result = subject.get_url_lookup(short_id: 'abc')

        expect(result).to eq(url_lookup)
        expect(UrlLookup).not_to have_received(:find_by)
      end
    end
  end
end
