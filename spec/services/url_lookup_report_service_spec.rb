require 'rails_helper'

RSpec.describe UrlLookupReportService do
  subject { described_class.new }

  describe '#process_report' do
    let(:csv_string_mock) { 'boo' }
    let(:s3_service) { instance_double(S3Service) }

    before do
      allow(S3Service).to receive(:new).and_return(s3_service)
      allow(s3_service).to receive(:upload)
    end

    it do
      subject.process_report

      expect(s3_service).to have_received(:upload)
    end
  end

  describe '#generate_report' do
    let(:ip_address_a) { '127.0.0.1' }
    let(:ip_address_b) { '127.0.0.2' }
    let(:country_a) { 'Malaysia' }
    let(:country_b) { 'Singapore' }
    let(:url_lookup_a) { create(:url_lookup) }
    let(:url_lookup_b) { create(:url_lookup) }
    let!(:url_lookup_metadata_aa) do
      create(:url_lookup_metadata, url_lookup: url_lookup_a, ip_address: ip_address_a, country: country_a)
    end
    let!(:url_lookup_metadata_ab) do
      create(:url_lookup_metadata, url_lookup: url_lookup_a, ip_address: ip_address_a, country: country_a)
    end
    let!(:url_lookup_metadata_ba) do
      create(:url_lookup_metadata, url_lookup: url_lookup_b, ip_address: ip_address_b, country: country_b)
    end

    it do
      result = subject.send(:generate_report)

      expect(result).to include('Short URL,Target URL,IP Address,Country,No. Of Clicks')
      expect(result).to include("#{url_lookup_a.short_url},#{url_lookup_a.target_url},#{ip_address_a},#{country_a},2")
      expect(result).to include("#{url_lookup_b.short_url},#{url_lookup_b.target_url},#{ip_address_b},#{country_b},1")
    end
  end
end
