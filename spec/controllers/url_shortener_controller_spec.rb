require 'rails_helper'

RSpec.describe UrlShortenerController, type: :request do
  describe 'GET #show' do
    let(:url_lookup) { build_stubbed(:url_lookup) }
    let(:mock_get_service) { instance_double(UrlLookupGetService, get_url_lookup: url_lookup) }
    let(:mock_metadata_service) { instance_double(UrlLookupMetadataService) }

    before do
      allow(UrlLookupGetService).to receive(:new).and_return(mock_get_service)
      allow(UrlLookupMetadataService).to receive(:new).and_return(mock_metadata_service)
      allow(mock_metadata_service).to receive(:record_visit)
    end

    it 'returns redirect to target url' do
      get url_shortener_path(short_id: 'abc')

      expect(mock_get_service).to have_received(:get_url_lookup).with(short_id: 'abc')
      expect(mock_metadata_service).to have_received(:record_visit).with(url_lookup:, ip_address: '127.0.0.1')

      expect(response).to have_http_status(:redirect).and redirect_to(url_lookup[:target_url])
    end
  end

  describe 'GET #new' do
    let(:created_url_lookup) { build_stubbed(:url_lookup) }
    let(:mock_get_service) { instance_double(UrlLookupGetService, get_url_lookup: created_url_lookup) }

    before do
      allow(UrlLookupGetService).to receive(:new).and_return(mock_get_service)
    end

    it do
      get root_path(created_short_url: created_url_lookup.short_url)

      expect(response).to have_http_status(:ok)
      expect(assigns(:created_url_lookup)).to eq(created_url_lookup)
      expect(assigns(:url_lookup)).to be_an_instance_of(UrlLookup)
    end
  end

  describe 'POST #create' do
    let(:url_lookup) { build_stubbed(:url_lookup) }
    let(:mock_create_service) { instance_double(UrlLookupCreateService) }

    before do
      allow(UrlLookupCreateService).to receive(:new).and_return(mock_create_service)
      allow(mock_create_service).to receive_messages(valid?: true, create!: url_lookup)
    end

    it do
      post url_shortener_index_path(url_lookup: { target_url: url_lookup.target_url })

      expect(response).to have_http_status(:found).and redirect_to(root_path(created_short_url: url_lookup[:short_url]))
    end

    context 'when create service is not valid' do
      before do
        allow(mock_create_service).to receive_messages(valid?: false, url_lookup:)
      end

      it do
        post url_shortener_index_path(url_lookup: { target_url: url_lookup.target_url })

        expect(response).to have_http_status(:bad_request).and render_template(:new)
      end
    end
  end
end
